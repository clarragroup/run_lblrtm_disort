#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 13:04:15 2019

@author: prowe

Purpose: Run DISORT using inputs specified in a namelist file

Instructions: 
    1) You will need to install f90nml:
       $ pip install f90nml
    2) Install rundisort_py
    3) Change the path below as needed for your setup.
    4) Make sure you have the file "sample.nml" in your path.
    5) Results should agree with those from sampleRun.
    
Copyright 2020 by Penny Rowe and NorthWest Research Associates     
"""

# .. Built-in modules
import numpy as np
import f90nml


class Inputs(object):
    '''
    Get inputs from namelist file and put into class
    '''
    
    __slots__ = ['out_dir',
                 'prof_dir',
                 'prof_file',
                 'od_file',
                 'lblrtm_dir',
                 'runlblrtm',
                 'solar_source_fun_file',
                 'surf_emiss_data_file',
                 'pmomfiles_liq',
                 'pmomfiles_ice',
                 'date_year_month_day',
                 'time_hour_min_sec',
                 'lat',
                 'lon',
                 'alt',
                 'view_angle',
                 'cld_od_vis_liq',
                 'cld_od_vis_ice',
                 'reff_wat',
                 'reff_ice',
                 'cloudbase_liq',
                 'cloudbase_ice',
                 'cloudtop_liq',
                 'cloudtop_ice',
                 'bwn',
                 'ewn',
                 'dnu',
                 'npts_lores_ifgram',
                 'dnu_lores',
                 'npts_padded_ifgram',
                 'liq_tdependence',
                 'ice_tdependence',  
                 'ssp_liq_temps',
                 'ssp_ice_temps',
                 'microwins',
                 'od_match',
                 'disort_nstr',
                 'disort_lamber',
                 'save_od',
                 'z_trop']

    def __init__(self, namelist):
        
        # .. Set the default values
        setattr(self, 'z_trop', 12)
        
        # .. Load in the namelist
        nml = f90nml.read(namelist)
        input_nml = nml['inputs_nml']
        
        
        # .. Correct the liq ssp files
        ssp_temps = 'ssp_liq_temps'
        pmomfiles = 'pmomfiles_liq'
        input_nml[pmomfiles] = []
        if type(input_nml[ssp_temps]) == int:
            Ntemps = 1
            input_nml[ssp_temps] = [input_nml[ssp_temps]]
        else:
            Ntemps = len(input_nml[ssp_temps])
        for i in range(Ntemps):
            pmomfile_name = pmomfiles + str(i)
            input_nml[pmomfiles].append(input_nml[pmomfile_name])
            del input_nml[pmomfile_name]
        
        
        # .. Correct the ice ssp files
        ssp_temps = 'ssp_ice_temps'
        pmomfiles = 'pmomfiles_ice'
        input_nml[pmomfiles] = []
        if type(input_nml[ssp_temps]) == int:
            Ntemps = 1
            input_nml[ssp_temps] = [input_nml[ssp_temps]]
        else:
            Ntemps = len(input_nml[ssp_temps])
        for i in range(Ntemps):
            pmomfile_name = pmomfiles + str(i)
            input_nml[pmomfiles].append(input_nml[pmomfile_name])
            del input_nml[pmomfile_name]
        
                
        # .. Correct the microwindows
        dum = np.asarray(input_nml['microwins'])
        dum.resize(int(len(dum)/2),2)
        input_nml['microwins'] = dum
    
            
        # .. Turn dictionary into class
        for key in input_nml:
            setattr(self, key, input_nml[key])
            
        # .. Round observer altitude to 3 decimal places
        self.alt = np.round(self.alt, 3)
            

